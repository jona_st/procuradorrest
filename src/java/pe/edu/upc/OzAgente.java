/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jsrro
 */
@Entity
@Table(name = "OzAgente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OzAgente.findAll", query = "SELECT o FROM OzAgente o")
    , @NamedQuery(name = "OzAgente.findByCoAgente", query = "SELECT o FROM OzAgente o WHERE o.coAgente = :coAgente")
    , @NamedQuery(name = "OzAgente.findByNoAgente", query = "SELECT o FROM OzAgente o WHERE o.noAgente = :noAgente")})
public class OzAgente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "co_agente")
    private Integer coAgente;
    @Size(max = 100)
    @Column(name = "no_agente")
    private String noAgente;
    @JoinTable(name = "OzSiniestroxAgente", joinColumns = {
        @JoinColumn(name = "co_agente", referencedColumnName = "co_agente")}, inverseJoinColumns = {
        @JoinColumn(name = "co_siniestro", referencedColumnName = "co_siniestro")})
    @ManyToMany
    private Collection<OzSinisestro> ozSinisestroCollection;

    public OzAgente() {
    }

    public OzAgente(Integer coAgente) {
        this.coAgente = coAgente;
    }

    public Integer getCoAgente() {
        return coAgente;
    }

    public void setCoAgente(Integer coAgente) {
        this.coAgente = coAgente;
    }

    public String getNoAgente() {
        return noAgente;
    }

    public void setNoAgente(String noAgente) {
        this.noAgente = noAgente;
    }

    @XmlTransient
    public Collection<OzSinisestro> getOzSinisestroCollection() {
        return ozSinisestroCollection;
    }

    public void setOzSinisestroCollection(Collection<OzSinisestro> ozSinisestroCollection) {
        this.ozSinisestroCollection = ozSinisestroCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (coAgente != null ? coAgente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OzAgente)) {
            return false;
        }
        OzAgente other = (OzAgente) object;
        if ((this.coAgente == null && other.coAgente != null) || (this.coAgente != null && !this.coAgente.equals(other.coAgente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.edu.upc.OzAgente[ coAgente=" + coAgente + " ]";
    }
    
}
