/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jsrro
 */
@Entity
@Table(name = "OzUsuario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OzUsuario.findAll", query = "SELECT o FROM OzUsuario o")
    , @NamedQuery(name = "OzUsuario.login", query = "SELECT o FROM OzUsuario o WHERE o.noUsuario = :noUsuario and o.txPassword = :txPassword")
    , @NamedQuery(name = "OzUsuario.findByCoUsuario", query = "SELECT o FROM OzUsuario o WHERE o.coUsuario = :coUsuario")
    , @NamedQuery(name = "OzUsuario.findByNoUsuario", query = "SELECT o FROM OzUsuario o WHERE o.noUsuario = :noUsuario")
    , @NamedQuery(name = "OzUsuario.findByTxPassword", query = "SELECT o FROM OzUsuario o WHERE o.txPassword = :txPassword")
    , @NamedQuery(name = "OzUsuario.findByFlActivo", query = "SELECT o FROM OzUsuario o WHERE o.flActivo = :flActivo")})
public class OzUsuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "co_usuario")
    private Integer coUsuario;
    @Size(max = 100)
    @Column(name = "no_usuario")
    private String noUsuario;
    @Size(max = 100)
    @Column(name = "tx_password")
    private String txPassword;
    @Size(max = 1)
    @Column(name = "fl_activo")
    private String flActivo;
    @ManyToMany(mappedBy = "ozUsuarioCollection")
    private Collection<OzRol> ozRolCollection;
    @JoinColumn(name = "co_procurador", referencedColumnName = "co_procurador")
    @ManyToOne
    private OzProcurador coProcurador;

    public OzUsuario() {
    }

    public OzUsuario(Integer coUsuario) {
        this.coUsuario = coUsuario;
    }

    public Integer getCoUsuario() {
        return coUsuario;
    }

    public void setCoUsuario(Integer coUsuario) {
        this.coUsuario = coUsuario;
    }

    public String getNoUsuario() {
        return noUsuario;
    }

    public void setNoUsuario(String noUsuario) {
        this.noUsuario = noUsuario;
    }

    public String getTxPassword() {
        return txPassword;
    }

    public void setTxPassword(String txPassword) {
        this.txPassword = txPassword;
    }

    public String getFlActivo() {
        return flActivo;
    }

    public void setFlActivo(String flActivo) {
        this.flActivo = flActivo;
    }

    @XmlTransient
    public Collection<OzRol> getOzRolCollection() {
        return ozRolCollection;
    }

    public void setOzRolCollection(Collection<OzRol> ozRolCollection) {
        this.ozRolCollection = ozRolCollection;
    }

    public OzProcurador getCoProcurador() {
        return coProcurador;
    }

    public void setCoProcurador(OzProcurador coProcurador) {
        this.coProcurador = coProcurador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (coUsuario != null ? coUsuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OzUsuario)) {
            return false;
        }
        OzUsuario other = (OzUsuario) object;
        if ((this.coUsuario == null && other.coUsuario != null) || (this.coUsuario != null && !this.coUsuario.equals(other.coUsuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.edu.upc.OzUsuario[ coUsuario=" + coUsuario + " ]";
    }
    
}
