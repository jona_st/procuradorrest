/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jsrro
 */
@Entity
@Table(name = "OzAseguradora")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OzAseguradora.findAll", query = "SELECT o FROM OzAseguradora o")
    , @NamedQuery(name = "OzAseguradora.findByCoAseguradora", query = "SELECT o FROM OzAseguradora o WHERE o.coAseguradora = :coAseguradora")
    , @NamedQuery(name = "OzAseguradora.findByTxRuc", query = "SELECT o FROM OzAseguradora o WHERE o.txRuc = :txRuc")
    , @NamedQuery(name = "OzAseguradora.findByTxRazonsocial", query = "SELECT o FROM OzAseguradora o WHERE o.txRazonsocial = :txRazonsocial")
    , @NamedQuery(name = "OzAseguradora.findByTxDireccion", query = "SELECT o FROM OzAseguradora o WHERE o.txDireccion = :txDireccion")
    , @NamedQuery(name = "OzAseguradora.findByTxTelefono", query = "SELECT o FROM OzAseguradora o WHERE o.txTelefono = :txTelefono")
    , @NamedQuery(name = "OzAseguradora.findByTxWs1", query = "SELECT o FROM OzAseguradora o WHERE o.txWs1 = :txWs1")
    , @NamedQuery(name = "OzAseguradora.findByTxWs2", query = "SELECT o FROM OzAseguradora o WHERE o.txWs2 = :txWs2")
    , @NamedQuery(name = "OzAseguradora.findByTxWs3", query = "SELECT o FROM OzAseguradora o WHERE o.txWs3 = :txWs3")
    , @NamedQuery(name = "OzAseguradora.findByTxWs4", query = "SELECT o FROM OzAseguradora o WHERE o.txWs4 = :txWs4")})
public class OzAseguradora implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "co_aseguradora")
    private Integer coAseguradora;
    @Size(max = 100)
    @Column(name = "tx_ruc")
    private String txRuc;
    @Size(max = 100)
    @Column(name = "tx_razonsocial")
    private String txRazonsocial;
    @Size(max = 300)
    @Column(name = "tx_direccion")
    private String txDireccion;
    @Size(max = 20)
    @Column(name = "tx_telefono")
    private String txTelefono;
    @Size(max = 300)
    @Column(name = "tx_ws1")
    private String txWs1;
    @Size(max = 300)
    @Column(name = "tx_ws2")
    private String txWs2;
    @Size(max = 300)
    @Column(name = "tx_ws3")
    private String txWs3;
    @Size(max = 300)
    @Column(name = "tx_ws4")
    private String txWs4;
    @OneToMany(mappedBy = "coAseguradora")
    private Collection<OzCliente> ozClienteCollection;

    public OzAseguradora() {
    }

    public OzAseguradora(Integer coAseguradora) {
        this.coAseguradora = coAseguradora;
    }

    public Integer getCoAseguradora() {
        return coAseguradora;
    }

    public void setCoAseguradora(Integer coAseguradora) {
        this.coAseguradora = coAseguradora;
    }

    public String getTxRuc() {
        return txRuc;
    }

    public void setTxRuc(String txRuc) {
        this.txRuc = txRuc;
    }

    public String getTxRazonsocial() {
        return txRazonsocial;
    }

    public void setTxRazonsocial(String txRazonsocial) {
        this.txRazonsocial = txRazonsocial;
    }

    public String getTxDireccion() {
        return txDireccion;
    }

    public void setTxDireccion(String txDireccion) {
        this.txDireccion = txDireccion;
    }

    public String getTxTelefono() {
        return txTelefono;
    }

    public void setTxTelefono(String txTelefono) {
        this.txTelefono = txTelefono;
    }

    public String getTxWs1() {
        return txWs1;
    }

    public void setTxWs1(String txWs1) {
        this.txWs1 = txWs1;
    }

    public String getTxWs2() {
        return txWs2;
    }

    public void setTxWs2(String txWs2) {
        this.txWs2 = txWs2;
    }

    public String getTxWs3() {
        return txWs3;
    }

    public void setTxWs3(String txWs3) {
        this.txWs3 = txWs3;
    }

    public String getTxWs4() {
        return txWs4;
    }

    public void setTxWs4(String txWs4) {
        this.txWs4 = txWs4;
    }

    @XmlTransient
    public Collection<OzCliente> getOzClienteCollection() {
        return ozClienteCollection;
    }

    public void setOzClienteCollection(Collection<OzCliente> ozClienteCollection) {
        this.ozClienteCollection = ozClienteCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (coAseguradora != null ? coAseguradora.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OzAseguradora)) {
            return false;
        }
        OzAseguradora other = (OzAseguradora) object;
        if ((this.coAseguradora == null && other.coAseguradora != null) || (this.coAseguradora != null && !this.coAseguradora.equals(other.coAseguradora))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.edu.upc.OzAseguradora[ coAseguradora=" + coAseguradora + " ]";
    }
    
}
