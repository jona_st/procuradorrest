/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jsrro
 */
@Entity
@Table(name = "OzProcurador")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OzProcurador.findAll", query = "SELECT o FROM OzProcurador o")
    , @NamedQuery(name = "OzProcurador.findByCoProcurador", query = "SELECT o FROM OzProcurador o WHERE o.coProcurador = :coProcurador")
    , @NamedQuery(name = "OzProcurador.findByNoProcurador", query = "SELECT o FROM OzProcurador o WHERE o.noProcurador = :noProcurador")})
public class OzProcurador implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "co_procurador")
    private Integer coProcurador;
    @Size(max = 100)
    @Column(name = "no_procurador")
    private String noProcurador;
    @OneToMany(mappedBy = "coProcurador")
    private Collection<OzUsuario> ozUsuarioCollection;
    @OneToMany(mappedBy = "coProcurador")
    private Collection<OzSinisestro> ozSinisestroCollection;

    public OzProcurador() {
    }

    public OzProcurador(Integer coProcurador) {
        this.coProcurador = coProcurador;
    }

    public Integer getCoProcurador() {
        return coProcurador;
    }

    public void setCoProcurador(Integer coProcurador) {
        this.coProcurador = coProcurador;
    }

    public String getNoProcurador() {
        return noProcurador;
    }

    public void setNoProcurador(String noProcurador) {
        this.noProcurador = noProcurador;
    }

    @XmlTransient
    public Collection<OzUsuario> getOzUsuarioCollection() {
        return ozUsuarioCollection;
    }

    public void setOzUsuarioCollection(Collection<OzUsuario> ozUsuarioCollection) {
        this.ozUsuarioCollection = ozUsuarioCollection;
    }

    @XmlTransient
    public Collection<OzSinisestro> getOzSinisestroCollection() {
        return ozSinisestroCollection;
    }

    public void setOzSinisestroCollection(Collection<OzSinisestro> ozSinisestroCollection) {
        this.ozSinisestroCollection = ozSinisestroCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (coProcurador != null ? coProcurador.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OzProcurador)) {
            return false;
        }
        OzProcurador other = (OzProcurador) object;
        if ((this.coProcurador == null && other.coProcurador != null) || (this.coProcurador != null && !this.coProcurador.equals(other.coProcurador))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.edu.upc.OzProcurador[ coProcurador=" + coProcurador + " ]";
    }
    
}
