/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jsrro
 */
@Entity
@Table(name = "OzRol")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OzRol.findAll", query = "SELECT o FROM OzRol o")
    , @NamedQuery(name = "OzRol.findByCoRol", query = "SELECT o FROM OzRol o WHERE o.coRol = :coRol")
    , @NamedQuery(name = "OzRol.findByNoRol", query = "SELECT o FROM OzRol o WHERE o.noRol = :noRol")})
public class OzRol implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "co_rol")
    private Integer coRol;
    @Size(max = 100)
    @Column(name = "no_rol")
    private String noRol;
    @JoinTable(name = "OzUsuarioxRol", joinColumns = {
        @JoinColumn(name = "co_rol", referencedColumnName = "co_rol")}, inverseJoinColumns = {
        @JoinColumn(name = "co_usuario", referencedColumnName = "co_usuario")})
    @ManyToMany
    private Collection<OzUsuario> ozUsuarioCollection;

    public OzRol() {
    }

    public OzRol(Integer coRol) {
        this.coRol = coRol;
    }

    public Integer getCoRol() {
        return coRol;
    }

    public void setCoRol(Integer coRol) {
        this.coRol = coRol;
    }

    public String getNoRol() {
        return noRol;
    }

    public void setNoRol(String noRol) {
        this.noRol = noRol;
    }

    @XmlTransient
    public Collection<OzUsuario> getOzUsuarioCollection() {
        return ozUsuarioCollection;
    }

    public void setOzUsuarioCollection(Collection<OzUsuario> ozUsuarioCollection) {
        this.ozUsuarioCollection = ozUsuarioCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (coRol != null ? coRol.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OzRol)) {
            return false;
        }
        OzRol other = (OzRol) object;
        if ((this.coRol == null && other.coRol != null) || (this.coRol != null && !this.coRol.equals(other.coRol))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.edu.upc.OzRol[ coRol=" + coRol + " ]";
    }
    
}
