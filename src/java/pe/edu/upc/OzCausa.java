/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jsrro
 */
@Entity
@Table(name = "OzCausa")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OzCausa.findAll", query = "SELECT o FROM OzCausa o")
    , @NamedQuery(name = "OzCausa.findByCoCausa", query = "SELECT o FROM OzCausa o WHERE o.coCausa = :coCausa")
    , @NamedQuery(name = "OzCausa.findByNoCausa", query = "SELECT o FROM OzCausa o WHERE o.noCausa = :noCausa")})
public class OzCausa implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "co_causa")
    private Integer coCausa;
    @Size(max = 100)
    @Column(name = "no_causa")
    private String noCausa;
    @JoinTable(name = "OzSiniestroxCausa", joinColumns = {
        @JoinColumn(name = "co_causa", referencedColumnName = "co_causa")}, inverseJoinColumns = {
        @JoinColumn(name = "co_siniestro", referencedColumnName = "co_siniestro")})
    @ManyToMany
    private Collection<OzSinisestro> ozSinisestroCollection;

    public OzCausa() {
    }

    public OzCausa(Integer coCausa) {
        this.coCausa = coCausa;
    }

    public Integer getCoCausa() {
        return coCausa;
    }

    public void setCoCausa(Integer coCausa) {
        this.coCausa = coCausa;
    }

    public String getNoCausa() {
        return noCausa;
    }

    public void setNoCausa(String noCausa) {
        this.noCausa = noCausa;
    }

    @XmlTransient
    public Collection<OzSinisestro> getOzSinisestroCollection() {
        return ozSinisestroCollection;
    }

    public void setOzSinisestroCollection(Collection<OzSinisestro> ozSinisestroCollection) {
        this.ozSinisestroCollection = ozSinisestroCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (coCausa != null ? coCausa.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OzCausa)) {
            return false;
        }
        OzCausa other = (OzCausa) object;
        if ((this.coCausa == null && other.coCausa != null) || (this.coCausa != null && !this.coCausa.equals(other.coCausa))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.edu.upc.OzCausa[ coCausa=" + coCausa + " ]";
    }
    
}
