/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jsrro
 */
@Entity
@Table(name = "OzSinisestro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OzSinisestro.findAll", query = "SELECT o FROM OzSinisestro o"),
        @NamedQuery(name = "OzSinisestro.findAllCond", query = "SELECT o FROM OzSinisestro o where o.coProcurador is null")
    , @NamedQuery(name = "OzSinisestro.findByCoProcurador", query = "SELECT o FROM OzSinisestro o WHERE o.coProcurador = :coProcurador")    
    , @NamedQuery(name = "OzSinisestro.findByCoSiniestro", query = "SELECT o FROM OzSinisestro o WHERE o.coSiniestro = :coSiniestro")
    , @NamedQuery(name = "OzSinisestro.findByFeCreacion", query = "SELECT o FROM OzSinisestro o WHERE o.feCreacion = :feCreacion")
    , @NamedQuery(name = "OzSinisestro.findByFeAtencion", query = "SELECT o FROM OzSinisestro o WHERE o.feAtencion = :feAtencion")
    , @NamedQuery(name = "OzSinisestro.findByTxCordenadas", query = "SELECT o FROM OzSinisestro o WHERE o.txCordenadas = :txCordenadas")
    , @NamedQuery(name = "OzSinisestro.findByFlEstado", query = "SELECT o FROM OzSinisestro o WHERE o.flEstado = :flEstado")
    , @NamedQuery(name = "OzSinisestro.findByFlZona", query = "SELECT o FROM OzSinisestro o WHERE o.flZona = :flZona")
    , @NamedQuery(name = "OzSinisestro.findByFlPresenciaPolicial", query = "SELECT o FROM OzSinisestro o WHERE o.flPresenciaPolicial = :flPresenciaPolicial")
    , @NamedQuery(name = "OzSinisestro.findByFlCamara", query = "SELECT o FROM OzSinisestro o WHERE o.flCamara = :flCamara")
    , @NamedQuery(name = "OzSinisestro.findByFlTipoPersona", query = "SELECT o FROM OzSinisestro o WHERE o.flTipoPersona = :flTipoPersona")
    , @NamedQuery(name = "OzSinisestro.findByFlDenunciaPolicial", query = "SELECT o FROM OzSinisestro o WHERE o.flDenunciaPolicial = :flDenunciaPolicial")
    , @NamedQuery(name = "OzSinisestro.findByNuDenunciaPolicial", query = "SELECT o FROM OzSinisestro o WHERE o.nuDenunciaPolicial = :nuDenunciaPolicial")
    , @NamedQuery(name = "OzSinisestro.findByFlConsecuencia", query = "SELECT o FROM OzSinisestro o WHERE o.flConsecuencia = :flConsecuencia")})
public class OzSinisestro implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "co_siniestro")
    private Integer coSiniestro;
    @Size(max = 100)
    @Column(name = "fe_creacion")
    private String feCreacion;
    @Size(max = 100)
    @Column(name = "fe_atencion")
    private String feAtencion;
    @Size(max = 200)
    @Column(name = "tx_cordenadas")
    private String txCordenadas;
    @Size(max = 1)
    @Column(name = "fl_estado")
    private String flEstado;
    @Size(max = 1)
    @Column(name = "fl_zona")
    private String flZona;
    @Size(max = 1)
    @Column(name = "fl_presencia_policial")
    private String flPresenciaPolicial;
    @Size(max = 1)
    @Column(name = "fl_camara")
    private String flCamara;
    @Size(max = 1)
    @Column(name = "fl_tipo_persona")
    private String flTipoPersona;
    @Size(max = 1)
    @Column(name = "fl_denuncia_policial")
    private String flDenunciaPolicial;
    @Size(max = 50)
    @Column(name = "nu_denuncia_policial")
    private String nuDenunciaPolicial;
    @Size(max = 1)
    @Column(name = "fl_consecuencia")
    private String flConsecuencia;
    @ManyToMany(mappedBy = "ozSinisestroCollection")
    private Collection<OzAgente> ozAgenteCollection;
    @ManyToMany(mappedBy = "ozSinisestroCollection")
    private Collection<OzInfraccion> ozInfraccionCollection;
    @ManyToMany(mappedBy = "ozSinisestroCollection")
    private Collection<OzSenal> ozSenalCollection;
    @ManyToMany(mappedBy = "ozSinisestroCollection")
    private Collection<OzCausa> ozCausaCollection;
    @JoinColumn(name = "co_procurador", referencedColumnName = "co_procurador")
    @ManyToOne
    private OzProcurador coProcurador;
    

    public OzSinisestro() {
    }

    public OzSinisestro(Integer coSiniestro) {
        this.coSiniestro = coSiniestro;
    }

    public Integer getCoSiniestro() {
        return coSiniestro;
    }

    public void setCoSiniestro(Integer coSiniestro) {
        this.coSiniestro = coSiniestro;
    }

    public String getFeCreacion() {
        return feCreacion;
    }

    public void setFeCreacion(String feCreacion) {
        this.feCreacion = feCreacion;
    }

    public String getFeAtencion() {
        return feAtencion;
    }

    public void setFeAtencion(String feAtencion) {
        this.feAtencion = feAtencion;
    }

    public String getTxCordenadas() {
        return txCordenadas;
    }

    public void setTxCordenadas(String txCordenadas) {
        this.txCordenadas = txCordenadas;
    }

    public String getFlEstado() {
        return flEstado;
    }

    public void setFlEstado(String flEstado) {
        this.flEstado = flEstado;
    }

    public String getFlZona() {
        return flZona;
    }

    public void setFlZona(String flZona) {
        this.flZona = flZona;
    }

    public String getFlPresenciaPolicial() {
        return flPresenciaPolicial;
    }

    public void setFlPresenciaPolicial(String flPresenciaPolicial) {
        this.flPresenciaPolicial = flPresenciaPolicial;
    }

    public String getFlCamara() {
        return flCamara;
    }

    public void setFlCamara(String flCamara) {
        this.flCamara = flCamara;
    }

    public String getFlTipoPersona() {
        return flTipoPersona;
    }

    public void setFlTipoPersona(String flTipoPersona) {
        this.flTipoPersona = flTipoPersona;
    }

    public String getFlDenunciaPolicial() {
        return flDenunciaPolicial;
    }

    public void setFlDenunciaPolicial(String flDenunciaPolicial) {
        this.flDenunciaPolicial = flDenunciaPolicial;
    }

    public String getNuDenunciaPolicial() {
        return nuDenunciaPolicial;
    }

    public void setNuDenunciaPolicial(String nuDenunciaPolicial) {
        this.nuDenunciaPolicial = nuDenunciaPolicial;
    }

    public String getFlConsecuencia() {
        return flConsecuencia;
    }

    public void setFlConsecuencia(String flConsecuencia) {
        this.flConsecuencia = flConsecuencia;
    }

    @XmlTransient
    public Collection<OzAgente> getOzAgenteCollection() {
        return ozAgenteCollection;
    }

    public void setOzAgenteCollection(Collection<OzAgente> ozAgenteCollection) {
        this.ozAgenteCollection = ozAgenteCollection;
    }

    @XmlTransient
    public Collection<OzInfraccion> getOzInfraccionCollection() {
        return ozInfraccionCollection;
    }

    public void setOzInfraccionCollection(Collection<OzInfraccion> ozInfraccionCollection) {
        this.ozInfraccionCollection = ozInfraccionCollection;
    }

    @XmlTransient
    public Collection<OzSenal> getOzSenalCollection() {
        return ozSenalCollection;
    }

    public void setOzSenalCollection(Collection<OzSenal> ozSenalCollection) {
        this.ozSenalCollection = ozSenalCollection;
    }

    @XmlTransient
    public Collection<OzCausa> getOzCausaCollection() {
        return ozCausaCollection;
    }

    public void setOzCausaCollection(Collection<OzCausa> ozCausaCollection) {
        this.ozCausaCollection = ozCausaCollection;
    }

    public OzProcurador getCoProcurador() {
        return coProcurador;
    }

    public void setCoProcurador(OzProcurador coProcurador) {
        this.coProcurador = coProcurador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (coSiniestro != null ? coSiniestro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OzSinisestro)) {
            return false;
        }
        OzSinisestro other = (OzSinisestro) object;
        if ((this.coSiniestro == null && other.coSiniestro != null) || (this.coSiniestro != null && !this.coSiniestro.equals(other.coSiniestro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.edu.upc.OzSinisestro[ coSiniestro=" + coSiniestro + " ]";
    }
    
}
