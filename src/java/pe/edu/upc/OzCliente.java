/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jsrro
 */
@Entity
@Table(name = "OzCliente")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OzCliente.findAll", query = "SELECT o FROM OzCliente o")
    , @NamedQuery(name = "OzCliente.findByCoCliente", query = "SELECT o FROM OzCliente o WHERE o.coCliente = :coCliente")
    , @NamedQuery(name = "OzCliente.findByTxNombre", query = "SELECT o FROM OzCliente o WHERE o.txNombre = :txNombre")
    , @NamedQuery(name = "OzCliente.findByTxApellido", query = "SELECT o FROM OzCliente o WHERE o.txApellido = :txApellido")
    , @NamedQuery(name = "OzCliente.findByTxDni", query = "SELECT o FROM OzCliente o WHERE o.txDni = :txDni")
    , @NamedQuery(name = "OzCliente.findByNuTelefono", query = "SELECT o FROM OzCliente o WHERE o.nuTelefono = :nuTelefono")
    , @NamedQuery(name = "OzCliente.findByNuSeguro", query = "SELECT o FROM OzCliente o WHERE o.nuSeguro = :nuSeguro")
    , @NamedQuery(name = "OzCliente.findByFlEstadosSeguro", query = "SELECT o FROM OzCliente o WHERE o.flEstadosSeguro = :flEstadosSeguro")
    , @NamedQuery(name = "OzCliente.findByTxPlaca", query = "SELECT o FROM OzCliente o WHERE o.txPlaca = :txPlaca")
    , @NamedQuery(name = "OzCliente.findByFlMora", query = "SELECT o FROM OzCliente o WHERE o.flMora = :flMora")})
public class OzCliente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "co_cliente")
    private Integer coCliente;
    @Size(max = 100)
    @Column(name = "tx_nombre")
    private String txNombre;
    @Size(max = 100)
    @Column(name = "tx_apellido")
    private String txApellido;
    @Size(max = 10)
    @Column(name = "tx_dni")
    private String txDni;
    @Size(max = 20)
    @Column(name = "nu_telefono")
    private String nuTelefono;
    @Size(max = 100)
    @Column(name = "nu_seguro")
    private String nuSeguro;
    @Size(max = 1)
    @Column(name = "fl_estados_seguro")
    private String flEstadosSeguro;
    @Size(max = 50)
    @Column(name = "tx_placa")
    private String txPlaca;
    @Size(max = 1)
    @Column(name = "fl_mora")
    private String flMora;
    @JoinColumn(name = "co_aseguradora", referencedColumnName = "co_aseguradora")
    @ManyToOne
    private OzAseguradora coAseguradora;

    public OzCliente() {
    }

    public OzCliente(Integer coCliente) {
        this.coCliente = coCliente;
    }

    public Integer getCoCliente() {
        return coCliente;
    }

    public void setCoCliente(Integer coCliente) {
        this.coCliente = coCliente;
    }

    public String getTxNombre() {
        return txNombre;
    }

    public void setTxNombre(String txNombre) {
        this.txNombre = txNombre;
    }

    public String getTxApellido() {
        return txApellido;
    }

    public void setTxApellido(String txApellido) {
        this.txApellido = txApellido;
    }

    public String getTxDni() {
        return txDni;
    }

    public void setTxDni(String txDni) {
        this.txDni = txDni;
    }

    public String getNuTelefono() {
        return nuTelefono;
    }

    public void setNuTelefono(String nuTelefono) {
        this.nuTelefono = nuTelefono;
    }

    public String getNuSeguro() {
        return nuSeguro;
    }

    public void setNuSeguro(String nuSeguro) {
        this.nuSeguro = nuSeguro;
    }

    public String getFlEstadosSeguro() {
        return flEstadosSeguro;
    }

    public void setFlEstadosSeguro(String flEstadosSeguro) {
        this.flEstadosSeguro = flEstadosSeguro;
    }

    public String getTxPlaca() {
        return txPlaca;
    }

    public void setTxPlaca(String txPlaca) {
        this.txPlaca = txPlaca;
    }

    public String getFlMora() {
        return flMora;
    }

    public void setFlMora(String flMora) {
        this.flMora = flMora;
    }

    public OzAseguradora getCoAseguradora() {
        return coAseguradora;
    }

    public void setCoAseguradora(OzAseguradora coAseguradora) {
        this.coAseguradora = coAseguradora;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (coCliente != null ? coCliente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OzCliente)) {
            return false;
        }
        OzCliente other = (OzCliente) object;
        if ((this.coCliente == null && other.coCliente != null) || (this.coCliente != null && !this.coCliente.equals(other.coCliente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.edu.upc.OzCliente[ coCliente=" + coCliente + " ]";
    }
    
}
