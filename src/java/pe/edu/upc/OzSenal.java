/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jsrro
 */
@Entity
@Table(name = "OzSenal")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OzSenal.findAll", query = "SELECT o FROM OzSenal o")
    , @NamedQuery(name = "OzSenal.findByCoSenal", query = "SELECT o FROM OzSenal o WHERE o.coSenal = :coSenal")
    , @NamedQuery(name = "OzSenal.findByNoSenal", query = "SELECT o FROM OzSenal o WHERE o.noSenal = :noSenal")})
public class OzSenal implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "co_senal")
    private Integer coSenal;
    @Size(max = 100)
    @Column(name = "no_senal")
    private String noSenal;
    @JoinTable(name = "OzSiniestroxSenal", joinColumns = {
        @JoinColumn(name = "co_senal", referencedColumnName = "co_senal")}, inverseJoinColumns = {
        @JoinColumn(name = "co_siniestro", referencedColumnName = "co_siniestro")})
    @ManyToMany
    private Collection<OzSinisestro> ozSinisestroCollection;

    public OzSenal() {
    }

    public OzSenal(Integer coSenal) {
        this.coSenal = coSenal;
    }

    public Integer getCoSenal() {
        return coSenal;
    }

    public void setCoSenal(Integer coSenal) {
        this.coSenal = coSenal;
    }

    public String getNoSenal() {
        return noSenal;
    }

    public void setNoSenal(String noSenal) {
        this.noSenal = noSenal;
    }

    @XmlTransient
    public Collection<OzSinisestro> getOzSinisestroCollection() {
        return ozSinisestroCollection;
    }

    public void setOzSinisestroCollection(Collection<OzSinisestro> ozSinisestroCollection) {
        this.ozSinisestroCollection = ozSinisestroCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (coSenal != null ? coSenal.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OzSenal)) {
            return false;
        }
        OzSenal other = (OzSenal) object;
        if ((this.coSenal == null && other.coSenal != null) || (this.coSenal != null && !this.coSenal.equals(other.coSenal))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.edu.upc.OzSenal[ coSenal=" + coSenal + " ]";
    }
    
}
