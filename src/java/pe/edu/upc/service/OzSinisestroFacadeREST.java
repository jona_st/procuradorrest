/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc.service;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import pe.edu.upc.OzProcurador;
import pe.edu.upc.OzSinisestro;
import pe.edu.upc.OzUsuario;

/**
 *
 * @author jsrro
 */
@Stateless
@Path("Siniestro")
public class OzSinisestroFacadeREST extends AbstractFacade<OzSinisestro> {

    @PersistenceContext(unitName = "ProcuradorRestPU")
    private EntityManager em;

    public OzSinisestroFacadeREST() {
        super(OzSinisestro.class);
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(OzSinisestro entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void edit(@PathParam("id") Integer id, OzSinisestro entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public OzSinisestro find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<OzSinisestro> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<OzSinisestro> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }
    
    
    
    @POST
    @Path("bandeja")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<OzSinisestro> findByCoProcurador(OzUsuario entity) {
        OzProcurador ozProcurador = new OzProcurador();
        ozProcurador.setCoProcurador(entity.getCoUsuario());
        List<OzSinisestro> lista = em.createNamedQuery("OzSinisestro.findByCoProcurador")
            .setParameter("coProcurador", ozProcurador)
            .getResultList();
        
        return lista;
    }
    
    
    @POST
    @Path("cola")
  
    @Produces(MediaType.APPLICATION_JSON)
    public List<OzSinisestro> findAllSiniestro() {
     
        List<OzSinisestro> lista = em.createNamedQuery("OzSinisestro.findAllCond")
           
            .getResultList();
        
        return lista;
    }
    
    
    
    @POST
    @Path("tomarSiniestro")
    @Consumes(MediaType.APPLICATION_JSON)
   
    @Produces(MediaType.APPLICATION_JSON)
    public Integer tomarSiniestro(OzSinisestro siniestroBean) {
     
        
        super.edit(siniestroBean);
      return 1;
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
