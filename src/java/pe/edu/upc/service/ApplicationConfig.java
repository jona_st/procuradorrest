/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc.service;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author jsrro
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(pe.edu.upc.service.OzAgenteFacadeREST.class);
        resources.add(pe.edu.upc.service.OzAseguradoraFacadeREST.class);
        resources.add(pe.edu.upc.service.OzCausaFacadeREST.class);
        resources.add(pe.edu.upc.service.OzClienteFacadeREST.class);
        resources.add(pe.edu.upc.service.OzInfraccionFacadeREST.class);
        resources.add(pe.edu.upc.service.OzProcuradorFacadeREST.class);
        resources.add(pe.edu.upc.service.OzRolFacadeREST.class);
        resources.add(pe.edu.upc.service.OzSenalFacadeREST.class);
        resources.add(pe.edu.upc.service.OzSinisestroFacadeREST.class);
        resources.add(pe.edu.upc.service.OzUsuarioFacadeREST.class);
    }
    
}
