/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.edu.upc;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jsrro
 */
@Entity
@Table(name = "OzInfraccion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "OzInfraccion.findAll", query = "SELECT o FROM OzInfraccion o")
    , @NamedQuery(name = "OzInfraccion.findByCoInfraccion", query = "SELECT o FROM OzInfraccion o WHERE o.coInfraccion = :coInfraccion")
    , @NamedQuery(name = "OzInfraccion.findByNoInfraccion", query = "SELECT o FROM OzInfraccion o WHERE o.noInfraccion = :noInfraccion")})
public class OzInfraccion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "co_infraccion")
    private Integer coInfraccion;
    @Size(max = 100)
    @Column(name = "no_infraccion")
    private String noInfraccion;
    @JoinTable(name = "OzSiniestroxInfraccion", joinColumns = {
        @JoinColumn(name = "co_infraccion", referencedColumnName = "co_infraccion")}, inverseJoinColumns = {
        @JoinColumn(name = "co_siniestro", referencedColumnName = "co_siniestro")})
    @ManyToMany
    private Collection<OzSinisestro> ozSinisestroCollection;

    public OzInfraccion() {
    }

    public OzInfraccion(Integer coInfraccion) {
        this.coInfraccion = coInfraccion;
    }

    public Integer getCoInfraccion() {
        return coInfraccion;
    }

    public void setCoInfraccion(Integer coInfraccion) {
        this.coInfraccion = coInfraccion;
    }

    public String getNoInfraccion() {
        return noInfraccion;
    }

    public void setNoInfraccion(String noInfraccion) {
        this.noInfraccion = noInfraccion;
    }

    @XmlTransient
    public Collection<OzSinisestro> getOzSinisestroCollection() {
        return ozSinisestroCollection;
    }

    public void setOzSinisestroCollection(Collection<OzSinisestro> ozSinisestroCollection) {
        this.ozSinisestroCollection = ozSinisestroCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (coInfraccion != null ? coInfraccion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OzInfraccion)) {
            return false;
        }
        OzInfraccion other = (OzInfraccion) object;
        if ((this.coInfraccion == null && other.coInfraccion != null) || (this.coInfraccion != null && !this.coInfraccion.equals(other.coInfraccion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "pe.edu.upc.OzInfraccion[ coInfraccion=" + coInfraccion + " ]";
    }
    
}
